//
//  ServicesModel.swift
//  TestTask
//
//  Created by N Sh on 17.06.2020.
//  Copyright © 2020 N Sh. All rights reserved.
//

import Foundation
import UIKit

struct ServicesModel{
    private(set) var categories: [String] = []
    private(set) var images: [UIImage] = []
    private(set) var items: [[String]] = []
    var count: Int {
        return categories.count
    }
    
    init(categories: [String], images: [UIImage], items: [[String]]) {
        self.categories = categories
        self.images = images
        self.items = items
    }
}
