//
//  CollapsableViewModel.swift
//  TestTask
//
//  Created by N Sh on 18.06.2020.
//  Copyright © 2020 N Sh. All rights reserved.
//

import Foundation
import UIKit

class CollapsableViewModel {
    let label: String
    let image: UIImage?
    let children: [CollapsableViewModel]
    var isExpandable: Bool
    
    init(label: String,
         image: UIImage? = nil,
         children: [CollapsableViewModel] = [],
         isCollapsed: Bool = false){
        self.label = label
        self.image = image
        self.children = children
        self.isExpandable = isCollapsed
    }
}
