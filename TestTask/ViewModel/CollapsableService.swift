//
//  CollapsableService.swift
//  TestTask
//
//  Created by N Sh on 18.06.2020.
//  Copyright © 2020 N Sh. All rights reserved.
//

import Foundation

class CollapsableService {
    func createCollapsableViewModelData(services: ServicesModel) -> [CollapsableViewModel]{
        var data: [CollapsableViewModel] = []
        for i in 0...services.count - 1{
            data.append(CollapsableViewModel(label: services.categories[i], image: services.images[i], children: createCollapsableViewModelFromSection(i: i, services: services)))
        }
        return data
    }
    
    private func createCollapsableViewModelFromSection(i: Int, services: ServicesModel) -> [CollapsableViewModel]{
        var arr: [CollapsableViewModel] = []
        for item in services.items[i]{
            arr.append(CollapsableViewModel(label: item))
        }
        return arr
    }
}
