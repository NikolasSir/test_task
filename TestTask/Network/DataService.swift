//
//  DataService.swift
//  TestTask
//
//  Created by N Sh on 16.06.2020.
//  Copyright © 2020 N Sh. All rights reserved.
//

import Foundation
import SwiftyJSON

struct DataService{
    private var networkService = NetworkService()
//    var delegate: URLSessionDelegate?
    
//    func getCertFromNetwork(callback: @escaping (Data?) -> Void){
//        self.networkService.getCert{ data in
//            DispatchQueue.global().async {
//                callback(data)
//            }
//        }
//    }
    
    func getServicesFromNetwork(callback: @escaping (ServicesModel) -> Void){
        self.networkService.getCatalog(delegate: networkService){ data in
            DispatchQueue.global(qos: .utility).async {
                let dataJSON = JSON(data as Any)
                let categories = dataJSON["data"].arrayValue.map{ $0["title"].stringValue }
                let icons = dataJSON["data"].arrayValue.map{ $0["icon"].intValue }
                let items = dataJSON["data"].arrayValue.map{ $0["items"].arrayValue.map{ $0["title"].stringValue } }
                self.getIconImages(icons: icons) { images in
                    let services = ServicesModel(categories: categories, images: images!, items: items)
                        callback(services)
                }
            }
        }
    }
        
    func getIconImages(icons: [Int], callback: @escaping ([UIImage]?)->Void){
        self.networkService.getIcons(icons: icons) { imagesData in
            DispatchQueue.global(qos: .utility).async{
                callback(imagesData)
            }
        }
    }
}
