//
//  NetworkService.swift
//  TestTask
//
//  Created by N Sh on 11.06.2020.
//  Copyright © 2020 N Sh. All rights reserved.
//

import Alamofire
import SwiftyJSON

class NetworkService: NSObject{

    private func getCert(callback: @escaping (Data?) -> Void){
       AF.request("https://authan-test.izibook.ru:13302/api/anonymous", method: .post, parameters: nil)
            .response{ response in
                switch response.result{
                case .success(let value):
                    let json = JSON(value as Any)
                    let data = json["data"]["pkcs12"]
                    let rawData = data.rawString()?.data(using: .utf8)
                    callback(rawData)
                case .failure(let error):
                    print(error.localizedDescription)
                    print("Cannot connect to server...")
                }
        }
    }
    
    func getCatalog(delegate: URLSessionDelegate, callback: @escaping (Data?) -> Void){
        let session = URLSession(configuration: .default, delegate: delegate, delegateQueue: nil)
        guard let url = URL(string: "https://rest-test.izibook.ru:10001/api/globcat/list") else {
                print("Cannot unwrap URL")
                return
        }
//      let json: [String: Any] = [ "filter": ["parent": 1],
//                                    "view": ["code": "icon",
//                                            "code": "id",
//                                            "code": "title",
//                                            "code": "popularity",
//                                            "code": ["items", "view" :
//                                                            ["code" : "id",
//                                                             "code" : "title",
//                                                             "code" : [ "items", "view" :
//                                                                         ["code" : "id",
//                                                                          "code" : "title"]]]]]]
        let jsonString = """
            {
                "filter": {
                    "parent": 1
                },
                "view": [
                    {
                        "code": "icon"
                    },
                    {
                        "code": "id"
                    },
                    {
                        "code": "title"
                    },
                    {
                        "code": "popularity"
                    },
                    {
                        "code": "items",
                        "view": [
                            {
                                "code": "id"
                            },
                            {
                                "code": "title"
                            },
                            {
                                "code": "items",
                                "view": [
                                    {
                                        "code": "id"
                                    },
                                    {
                                        "code": "title"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        """
        let jsonData = JSON(jsonString)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("x-lang=ru-RU", forHTTPHeaderField: "Content-Language")
        let requestData = jsonData.rawString()!.data(using: .utf8)! as Data
        request.httpBody = requestData
        session.dataTask(with: request){ data, _, error in
            if error != nil {
                print(error!.localizedDescription)
            }
            callback(data)
        }
        .resume()
    }
    
    func getIcons(icons: [Int], callback: @escaping ([UIImage]?) -> Void){
        var imagesDataDict: [Int: UIImage] = [:]
        var imagesData: [UIImage] = []
        for icon in icons{
            DispatchQueue.global(qos: .utility).async {
                AF.request("http://mi-test.izibook.ru/imagemanager/manager/singleget?&image=\(icon)&h=45&w=45", method: .get, parameters: nil).response{ response in
                    switch response.result{
                    case .success(let value):
                        guard let data = value else {
                            print("Cannot get data")
                            return
                        }
                        guard let image = UIImage(data: data) else {
                            print("Can't get image")
                            return
                        }
                        imagesDataDict[icon] = image
                        if imagesDataDict.count == icons.count{
                            for icon in icons{
                                imagesData.append(imagesDataDict[icon]!)
                            }
                            callback(imagesData)
                        }
                    case .failure(let error):
                        print(error.localizedDescription)
                    }
                }
            }
        }
    }
}

extension NetworkService: URLSessionDelegate{
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        getCert() { data in
            guard let data = data else { return }
            if let p12Data = Data(base64Encoded: data){
                let certData = PKCS12(PKCS12Data: p12Data as NSData)
                let credential = URLCredential(PKCS12: certData)
                completionHandler(.useCredential, credential)
            }
        }
    }
}


